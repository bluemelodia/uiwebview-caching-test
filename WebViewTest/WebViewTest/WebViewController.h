//
//  WebViewController.h
//  WebViewTest
//
//  Created by Melanie Lislie Hsu on 8/24/15.
//  Copyright (c) 2015 Melanie Lislie Hsu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASIWebPageRequest.h"

@interface WebViewController : UIViewController <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak,nonatomic) ASIWebPageRequest *request;


@end
