//
//  WebViewController.m
//  WebViewTest
//
//  Created by Melanie Lislie Hsu on 8/24/15.
//  Copyright (c) 2015 Melanie Lislie Hsu. All rights reserved.
//

#import "WebViewController.h"
#import "ASIDownloadCache.h"

@interface WebViewController ()

@end

@implementation WebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onLoadButtonPress:(id)sender {
    NSString *pdf = @"https://drive.google.com/file/d/0B6yHjFkNymJoNmlNLTdBSHVBbzQ/view?usp=sharing";
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:pdf]];
    [self.webView loadRequest:request];
}

- (IBAction)loadURL:(NSURL *)url
{
    // Assume request is a property of our controller
    // First, we'll cancel any in-progress page load
    [[self request] setDelegate:nil];
    [[self request] cancel];
    
    [self setRequest:[ASIWebPageRequest requestWithURL:url]];
    [[self request] setDelegate:self];
    [[self request] setDidFailSelector:@selector(webPageFetchFailed:)];
    [[self request] setDidFinishSelector:@selector(webPageFetchSucceeded:)];
    
    // Tell the request to embed external resources directly in the page
    [[self request] setUrlReplacementMode:ASIReplaceExternalResourcesWithData];
    
    // It is strongly recommended you use a download cache with ASIWebPageRequest
    // When using a cache, external resources are automatically stored in the cache
    // and can be pulled from the cache on subsequent page loads
    [[self request] setDownloadCache:[ASIDownloadCache sharedCache]];
    
    // Ask the download cache for a place to store the cached data
    // This is the most efficient way for an ASIWebPageRequest to store a web page
    [[self request] setDownloadDestinationPath:
     [[ASIDownloadCache sharedCache] pathToStoreCachedResponseDataForRequest:[self request]]];
    
    [[self request] startAsynchronous];
}

- (void)webPageFetchFailed:(ASIHTTPRequest *)theRequest {
    // Obviously you should handle the error properly...
    NSLog(@"%@",[theRequest error]);
}

- (void)webPageFetchSucceeded:(ASIHTTPRequest *)theRequest {
    NSString *response = [NSString stringWithContentsOfFile:
                          [theRequest downloadDestinationPath] encoding:[theRequest responseEncoding] error:nil];
    // Note we're setting the baseURL to the url of the page we downloaded. This is important!
    [webView loadHTMLString:response baseURL:[request url]];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
